const Apify = require('apify');

Apify.main(async () => {
    const input = await Apify.getInput();
    if (!input.products) throw new Error('products not defined, please check your input.')

    for (const product of input.products) {
        const offers = product.offers;
        const cheapestOffer = offers.reduce((prev, curr) => {
            const prevPrice = Number.parseFloat(prev.offer.replace('$', ''))
            const currPrice = Number.parseFloat(curr.offer.replace('$', ''))
            return currPrice < prevPrice ? curr : prev;
        });
        product.offers = [cheapestOffer]
        await Apify.pushData(product)
    }

    console.log('Done.');
});
